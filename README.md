# Ionic Event Tracking

Integration with event trackings as Google Analytics and Mixpanel.

## Requirements

* Ionic 3.9

## Installation

```bash
npm install --dev @c4uno/ionic-event-tracking
```

To use Google Analytics install, it is necessary install the Cordova plugin

```bash
ionic cordova plugin add cordova-plugin-google-analytics
```

## Use

Import and configure the `EventTrackingModule` in you `AppModule`

```typescript
import {EventTrackingModule} from "@c4uno/ionic-event-tracking";

@NgModule({
  imports: [
    EventTrackingModule.forRoot({
      enabled: true,
      trackingId: '123456'
    }),
  ]
})
```

### To track page view

Use the decorator `PageTrack`

```typescript
import {PageTrack} from "@c4uno/ionic-event-tracking";

@PageTrack('Contacto')
export class ContactPage {
    
}

```

### To track custom events
Use the `EventTracking` service

```typescript
import {EventTracking} from '@c4uno/ionic-event-tracking';

export class LoginPage {

  @Input() cellphone: string = '';
  @Input() password: string = '';

  /**
   *
   * @param {NavController} navCtrl
   * @param {AuthProvider} auth
   * @param analyticsService
   */
  constructor(private eventTracking: EventTracking) {
  }
  
  public login() {
      this.eventTracking.trackEvent('login', 'success');
  }
}
``` 