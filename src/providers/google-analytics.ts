import {Injectable} from '@angular/core';
import {GoogleAnalytics} from "@ionic-native/google-analytics";
import {EventTracking} from "..";
import {EventTrackingConfig} from "../event-tracking.module";

@Injectable()
export class GoogleAnalyticsProvider implements EventTracking {
    /**
     *
     * @param ga
     * @param config
     */
    constructor(private ga: GoogleAnalytics, config: EventTrackingConfig) {
        this.ga.startTrackerWithId(config.trackingId)
            .then(() => console.log('GA Tracking loaded'));
    };

    /**
     * @param page
     */
    visit(page: string) {
        this.ga.trackView(page);
    }

    /**
     *
     * @param category
     * @param action
     */
    trackEvent(category: string, action: string) {
        this.ga.trackEvent(category, action);
    }
}
