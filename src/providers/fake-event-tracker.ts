import {EventTracking} from "..";

/**
 *
 */
export class FakeEventTracker implements EventTracking {
    trackEvent(category: string, action: string) {
        console.log('Tracking event:', category, action);
    }

    visit(page: string) {
        console.log('Tracking event: visit -> ', page)
    }
}
