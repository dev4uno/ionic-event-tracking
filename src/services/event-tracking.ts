export abstract class EventTracking {
    /**
     *
     * @param {string} page
     */
    abstract visit(page: string);

    /**
     *
     * @param category
     * @param {string} action
     */
    abstract trackEvent(category: string, action: string);
}
