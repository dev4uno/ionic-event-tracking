import {EventTracking} from "..";
import {EventTrackingModule} from "../event-tracking.module";

/**
 *
 * @param {string} pageName
 * @returns {ClassDecorator}
 * @constructor
 */
export function PageTrack(pageName: string): ClassDecorator {
    return function (constructor: any) {
        const originalIonViewDidEnter = constructor.prototype.ionViewDidEnter;

        constructor.prototype.ionViewDidEnter = function (...args) {
            const eventTracking = EventTrackingModule.injector.get(EventTracking);

            eventTracking.visit(pageName);
            originalIonViewDidEnter && originalIonViewDidEnter.apply(this, args);
        }
    }
}
