import {Injector, ModuleWithProviders, NgModule} from '@angular/core';
import {GoogleAnalytics} from "@ionic-native/google-analytics";
import {EventTracking} from "./services/event-tracking";
import {GoogleAnalyticsProvider} from "./providers/google-analytics";
import {FakeEventTracker} from "./providers/fake-event-tracker";


@NgModule()
export class EventTrackingModule {

    static injector: Injector;

    /**
     *
     * @param injector
     */
    constructor(injector: Injector) {
        EventTrackingModule.injector = injector;
    }

    static forRoot(config): ModuleWithProviders {

        return {
            ngModule: EventTrackingModule,
            providers: [
                GoogleAnalytics,
                {
                    provide: EventTracking,
                    useFactory: eventTrackingFactory,
                    deps: [
                        GoogleAnalytics,
                        EventTrackingConfig,
                    ]
                },
                {
                    provide: EventTrackingConfig,
                    useValue: config
                }
            ]
        }
    }
}

/**
 *
 * @param ga
 * @param config
 */
export function eventTrackingFactory(ga: GoogleAnalytics, config: EventTrackingConfig) {
    if (config.enabled) {
        return new GoogleAnalyticsProvider(ga, config);
    }

    return new FakeEventTracker();
}

/**
 *
 */
export class EventTrackingConfig {
    enabled: boolean;
    trackingId: string;
}
